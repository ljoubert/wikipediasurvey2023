bdd_reference <- bdd

ip_lst = bdd_reference[,5]

coord_ville <- maxmind(ip_lst, "GeoLite2-City.mmdb", # geolocation database based on IP address
                       fields=c("country_name", "region_name", "city_name"))
colnames(coord_ville) <- c("country","region","city")

bdd_finale <- cbind(bdd_reference,coord_ville)

# data organization
bdd_finale <- bdd_finale %>% relocate(country, .after=ipaddr) %>% relocate(region, .after=country) %>% relocate(city, .after=region)

# slow code depending on database size
# TODO : accélérer
# coord_ville <- geolocate(bdd[,5])
# 
# for (i in 1:nrow(bdd)){
#   if (coord_ville[i,1]!= "success") {
#     coord_ville [i,] <- geolocate(bdd[i,5])
#     Sys.sleep(1) # pause d'une seconde entre chaque ligne
#   }
# 
# }


# Correlation between country of IP address and country selected (G06Q74) --------

bdd_corr <- bdd_finale
bdd_corr$G06Q74 <- as.character(bdd_corr$G06Q74)
bdd_corr$G06Q74 <- factor(bdd_corr$G06Q74, levels=unlist(dico_pays[,3]))

bdd_corr <- changement_modalites(bdd_corr, "G06Q74", "G06Q74", unlist(dico_pays[,2]),"Afghanistan")

x <- as.character(bdd_corr$G06Q74)
y <- as.character(bdd_corr$country)
tab <-table(x,y, useNA = 'ifany')
assocstats(tab)


# Create new country variable ---------------------------------------------

# slow code depending on database size

bdd_finale$country_bis <- bdd_finale$G06Q74

for (i in 1:nrow(bdd_finale)) {
  if (is.na(bdd_finale$country_bis[i])) {
    bdd_finale$country_bis[i] <- dico_pays[dico_pays[, 2] == bdd_finale$country[i], 4]
  }
}

bdd_finale$country_bis <- factor(bdd_finale$country_bis,levels = unlist(dico_pays[,3]))
bdd_finale$country_bis <- as.character(bdd_finale$country_bis)

# data organization
# TODO : debug
# nouveau_code_var <- as.vector(dict_variables[,2])
# nouveau_code_var <- unlist(nouveau_code_var)
# bdd_finale <- bdd_finale[,nouveau_code_var]

bdd <- bdd_finale



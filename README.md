# WikipediaSurvey2023

Repository aiming to provide reproductible processing of WikipediaSurvey2023 from raw data to paper-ready dataset.

**code not working yet**

## Files

- (01 to 19)**.R : module of data processing
- main.R : combination of modules to make a dataset
- GeoLite2-City.mmdb, dico_pays.xlsx, wikipedia_dico_variables.xlsx : assets of modules

## Guidelines to make a module

- each file have to start from a "bdd" objet and yield a "bdd" object, to ensure combination
- no package loading into module (now all loadings into main, have to be externalized)

## TODO

- make the code work :)
- make a loadings module to load libraries AND marsouin functions
- document dependencies between modules
- send Hypotheses tutorials about Git to the whole team
- add dico Country from Nicolas
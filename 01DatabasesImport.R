

# turkish database
bdd_tr <- read.csv("results_survey_turkish.csv")

# ukrainian database
bdd_uk <- read.csv("results_survey_ukrainian.csv")

# french databases
bdd_fr_partie1 <- read.csv("results_survey_french1.csv")
bdd_fr_partie2 <- read.csv("results_survey_french2.csv")
bdd_fr_partie1 <- bdd_fr_partie1[,-c(401:508)]
bdd_fr_partie2 <- bdd_fr_partie2[,-c(401:510)]
bdd_fr <- rbind(bdd_fr_partie1, bdd_fr_partie2)

# english database
bdd_en <- read.csv("results_survey_english.csv")

# italian database
bdd_it <- read.csv("results_survey_italian.csv")

# german database
bdd_de <- read.csv("results_survey_german.csv")

# portuguese database
bdd_pt <- read.csv("results_survey_portuguese.csv")

# spanish database
bdd_es <- read.csv("results_survey_spanish.csv")

# dictionaries
dict_variables <- read_excel("wikipedia_dico_variables.xlsx", col_names = TRUE)
dico_pays <- read_excel("dico_pays.xlsx", col_names = TRUE)


############################
##### DELETE VARIABLES #####
############################

# delete unwanted and empty variables
bdd_fr <- bdd_fr[,-c(1,2,6,7,32,58,137,332,347)]
bdd_en <- bdd_en[,-c(1,2,6,7,32,57,135,330,345,399:506)]
bdd_tr <- bdd_tr[,-c(1,2,6,7,32,58,137,332,347,401:508)]
bdd_it <- bdd_it[,-c(1,2,6,7,32,58,137,332,347,401:510)]
bdd_uk <- bdd_uk[,-c(1,2,6,7,32,58,137,332,347,401:510)]
bdd_de <- bdd_de[,-c(1,2,6,7,32,58,137,332,347,401:512)]
bdd_es <- bdd_es[,-c(1,2,6,7,32,58,137,332,347,401:512)]
bdd_pt <- bdd_pt[,-c(1,2,6,7,32,58,137,332,347,401:512)]

##########################
### RECODING VARIABLES ###
##########################

# add missing variables to the English database
bdd_en$`G2Q00019.SQ001.` <- bdd_en$`G2Q00019.SQ002.`
bdd_en$`G2Q00024.SQ001.` <- bdd_en$`G2Q00024.SQ002.`

# standardization of variable codes
ancien_code_var <- colnames(bdd_fr)
colnames(bdd_tr) <- c(ancien_code_var)
colnames(bdd_it) <- c(ancien_code_var)
colnames(bdd_uk) <- c(ancien_code_var)
colnames(bdd_de) <- c(ancien_code_var)
colnames(bdd_pt) <- c(ancien_code_var)
colnames(bdd_es) <- c(ancien_code_var)

# add language variables
bdd_fr$questlanguage <-rep("fr",nrow(bdd_fr))
bdd_en$questlanguage <-rep("en",nrow(bdd_en))
bdd_tr$questlanguage <-rep("tr",nrow(bdd_tr))
bdd_it$questlanguage <-rep("it",nrow(bdd_it))
bdd_uk$questlanguage <-rep("uk",nrow(bdd_uk))
bdd_de$questlanguage <-rep("de",nrow(bdd_de))
bdd_es$questlanguage <-rep("es",nrow(bdd_es))
bdd_pt$questlanguage <-rep("pt",nrow(bdd_pt))

# arrange database variables
ancien_code_var <- as.vector(dict_variables[-(7:22),1])
ancien_code_var <- unlist(ancien_code_var)
bdd_en <- bdd_en[,ancien_code_var]
bdd_tr <- bdd_tr[,ancien_code_var]
bdd_fr <- bdd_fr[,ancien_code_var]
bdd_it <- bdd_it[,ancien_code_var]
bdd_uk <- bdd_uk[,ancien_code_var]
bdd_de <- bdd_de[,ancien_code_var]
bdd_es <- bdd_es[,ancien_code_var]
bdd_pt <- bdd_pt[,ancien_code_var]

# rename database variables
nouveau_code_var <- as.vector(dict_variables[-(7:22),2])
nouveau_code_var <- unlist(nouveau_code_var)
colnames(bdd_fr) <- c(nouveau_code_var)
colnames(bdd_tr) <- c(nouveau_code_var)
colnames(bdd_en) <- c(nouveau_code_var)
colnames(bdd_it) <- c(nouveau_code_var)
colnames(bdd_uk) <- c(nouveau_code_var)
colnames(bdd_de) <- c(nouveau_code_var)
colnames(bdd_es) <- c(nouveau_code_var)
colnames(bdd_pt) <- c(nouveau_code_var)


# database merging
bdd <- rbind(bdd_fr,bdd_en,bdd_tr,bdd_it,bdd_uk,bdd_de,bdd_es,bdd_pt)

# replace empty data with NA
bdd[ bdd == "" ] <- NA
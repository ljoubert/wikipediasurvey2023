# recovery of questionnaire language variables, contact reasons, IDs and e-mail addresses
table_relance <- data.frame(bdd$questlanguage,bdd$G07Q90,bdd$G07Q91,bdd$G07Q92,bdd$G07Q93,bdd$G02Q25,bdd$G04Q52)

# we keep in our follow-up tracking table the individuals who have answered at least one of the questions about a possible contact
table_relance <- subset(table_relance, (bdd$G07Q90 == "Y" | bdd$G07Q92 == "Y" | bdd$G07Q91 == "Y"))

# if the e-mail address is missing, we replace the empty data with the reader pseudonym. If the reader pseudonym is missing, we replace the empty data with the contributor pseudonym
table_relance$bdd.G07Q93 <- ifelse(is.na(table_relance$bdd.G07Q93), table_relance$bdd.G02Q25, table_relance$bdd.G07Q93)
table_relance$bdd.G07Q93 <- ifelse(is.na(table_relance$bdd.G07Q93), table_relance$bdd.G04Q52, table_relance$bdd.G07Q93)

# we delete reader and contributor pseudonym variables
table_relance <- table_relance[,-c(6,7)]

# we only keep individuals with an e-mail address or a pseudonym
table_relance <- table_relance[!is.na(table_relance$bdd.G07Q93),]

# rename columns
colnames(table_relance) <- c("Survey langage"," Would you like to be kept informed of the results of the survey? "," Would you agree to answer to another survey in one year approximatly? ", "Would you be willing to be contacted for an interview by phone or video conference of about 1 hour about Wikipedia?","email")

# export the follow-up tracking table in xlsx format
rownames(table_relance) <- NULL
openxlsx::write.xlsx(table_relance, "follow_up_table.xlsx")
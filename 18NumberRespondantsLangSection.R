library(questionr)

# Language ----------------------------------------------------------------

# Number of people who clicked on the banner
freq(bdd_reference$questlanguage)

# Number of people who accessed the questionnaire
freq(bdd_repondants_anonym$questlanguage)


# Partie 1 ----------------------------------------------------------------

# Removal of individuals who answered less than one question for part one
nb_rep <- apply(X = !is.na(bdd_repondants_anonym[,22:40]), MARGIN = 1, FUN = sum)
nb_rep <- as.data.frame(nb_rep)
rownames(nb_rep) <- 1:100332
nb_ligne <- which(nb_rep>1)
bdd_repondants_anonym_part_01 <- bdd_repondants_anonym[nb_ligne,]

# Number of individuals by questionnaire language who answered at least one question for part one
freq(bdd_repondants_anonym_part_01$questlanguage)

# Partie 2 ----------------------------------------------------------------

# Removal of individuals who answered less than one question for part two
nb_rep <- apply(X = !is.na(bdd_repondants_anonym[,41:102]), MARGIN = 1, FUN = sum)
nb_rep <- as.data.frame(nb_rep)
rownames(nb_rep) <- 1:100332
nb_ligne <- which(nb_rep>1)
bdd_repondants_anonym_part_02 <- bdd_repondants_anonym[nb_ligne,]

# Number of individuals by questionnaire language who answered at least one question for part two
freq(bdd_repondants_anonym_part_02$questlanguage)

# Partie 3 ----------------------------------------------------------------

# Removal of individuals who answered less than one question for part three
nb_rep <- apply(X = !is.na(bdd_repondants_anonym[,103:135]), MARGIN = 1, FUN = sum)
nb_rep <- as.data.frame(nb_rep)
rownames(nb_rep) <- 1:100332
nb_ligne <- which(nb_rep>1)
bdd_repondants_anonym_part_03 <- bdd_repondants_anonym[nb_ligne,]

# Number of individuals by questionnaire language who answered at least one question for part three
freq(bdd_repondants_anonym_part_03$questlanguage)

# Partie 4 ----------------------------------------------------------------

# Removal of individuals who answered less than one question for part four
nb_rep <- apply(X = !is.na(bdd_repondants_anonym[,136:146]), MARGIN = 1, FUN = sum)
nb_rep <- as.data.frame(nb_rep)
rownames(nb_rep) <- 1:100332
nb_ligne <- which(nb_rep>1)
bdd_repondants_anonym_part_04 <- bdd_repondants_anonym[nb_ligne,]

# Number of individuals by questionnaire language who answered at least one question for part four
freq(bdd_repondants_anonym_part_04$questlanguage)

# Partie 5 ----------------------------------------------------------------

# Removal of individuals who answered less than one question for part five
nb_rep <- apply(X = !is.na(bdd_repondants_anonym[,202:227]), MARGIN = 1, FUN = sum)
nb_rep <- as.data.frame(nb_rep)
rownames(nb_rep) <- 1:100332
nb_ligne <- which(nb_rep>1)
bdd_repondants_anonym_part_05 <- bdd_repondants_anonym[nb_ligne,]

# Number of individuals by questionnaire language who answered at least one question for part five
freq(bdd_repondants_anonym_part_05$questlanguage)

# Partie 6 ----------------------------------------------------------------

# Removal of individuals who answered less than one question for part six
nb_rep <- apply(X = !is.na(bdd_repondants_anonym[,340:363]), MARGIN = 1, FUN = sum)
nb_rep <- as.data.frame(nb_rep)
rownames(nb_rep) <- 1:100332
nb_ligne <- which(nb_rep>1)
bdd_repondants_anonym_part_06 <- bdd_repondants_anonym[nb_ligne,]

# Number of individuals by questionnaire language who answered at least one question for part six
freq(bdd_repondants_anonym_part_06$questlanguage)

# Partie 7 ----------------------------------------------------------------

# Removal of individuals who answered less than one question for part seven
nb_rep <- apply(X = !is.na(bdd_repondants_anonym[,364:380]), MARGIN = 1, FUN = sum)
nb_rep <- as.data.frame(nb_rep)
rownames(nb_rep) <- 1:100332
nb_ligne <- which(nb_rep>1)
bdd_repondants_anonym_part_07 <- bdd_repondants_anonym[nb_ligne,]

# Number of individuals by questionnaire language who answered at least one question for part seven
freq(bdd_repondants_anonym_part_07$questlanguage)

# Partie 8 ----------------------------------------------------------------

# Removal of individuals who answered less than one question for part eight
nb_rep <- apply(X = !is.na(bdd_repondants_anonym[,381:403]), MARGIN = 1, FUN = sum)
nb_rep <- as.data.frame(nb_rep)
rownames(nb_rep) <- 1:100332
nb_ligne <- which(nb_rep>1)
bdd_repondants_anonym_part_08 <- bdd_repondants_anonym[nb_ligne,]

# Number of individuals by questionnaire language who answered at least one question for part eight
freq(bdd_repondants_anonym_part_08$questlanguage)
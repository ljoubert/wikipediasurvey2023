
bdd_primaire <- bdd

# Platform ----------------------------------------------------------------

# index of url containing "questionnaires.marsouin"
num_marsouin <- grep(pattern="questionnaires.marsouin", x=bdd_primaire$refurl, value=FALSE)

# index of url containing "m"
num_m <- grep(pattern="m", x=bdd_primaire$refurl, value=FALSE)

# index of url containing "m" without "questionnaires.marsouin"
num_platform <- num_m[which(!num_m %in% num_marsouin)]

# initialization of the "platform" variable
bdd_primaire$platform <- rep(NA, nrow(bdd_primaire))

# modality "AO01" if the individual completed the questionnaire from a cell phone
bdd_primaire$platform[num_platform] <- "AO01"

# modality "AO02" otherwise
bdd_primaire$platform[-num_platform] <- "AO02"

# organize variables according to dictionary order. Place the platform variable after the url variable
bdd_reference <- bdd_primaire %>% relocate(platform, .after=refurl)


# Reader_id ---------------------------------------------------------------

# initialization of the "reader_id" variable
bdd_reference$reader_id <- rep(NA, nrow(bdd_reference))

# modality "AO01" if the individual has completed the reader pseudonym question, "AO02" otherwise
for (i in 1:nrow(bdd_reference)) {
  if (!is.na(bdd_reference$G02Q25[i])) {
    bdd_reference$reader_id[i] <- "AO01"
    
  } else {
    bdd_reference$reader_id[i] <- "AO02"
  }
}


# Contrib_id --------------------------------------------------------------

# initialization of the "contrib_id" variable
bdd_reference$contrib_id <- rep(NA, nrow(bdd_reference))

# modality "AO01" if the individual has completed the contributor pseudonym question, "AO02" otherwise
for (i in 1:nrow(bdd_reference)) {
  if (!is.na(bdd_reference$G04Q52[i])) {
    bdd_reference$contrib_id[i] <- "AO01"
    
  } else {
    bdd_reference$contrib_id[i] <- "AO02"
  }
}


# email -------------------------------------------------------------------

# initialization of the "email" variable
bdd_reference$email <- rep(NA, nrow(bdd_reference))

# modality "AO01" if the individual has given his e-mail address, "AO02" otherwise
for (i in 1:nrow(bdd_reference)) {
  if (!is.na(bdd_reference$G07Q93[i])) {
    bdd_reference$email[i] <- "AO01"
    
  } else {
    bdd_reference$email[i] <- "AO02"
  }
}


# occupational_group ------------------------------------------------------

# classification of occupational groups
classes <- c("AO01","AO02","AO03","AO05","AO07","AO10","AO04","AO06","AO08","AO09","AO11")
echelle <- c(1,1,2,2,2,2,3,3,3,3,4)
classe <- data.frame(classes,echelle)

##########################
### highest profession ###
##########################

bdd_reference$CSP_highest <- rep(NA, nrow(bdd_reference))

for (i in 1:nrow(bdd_reference)) {
  # we test whether the questions on father's and mother's occupational group are not both null
  if (!is.na(bdd_reference$`G06Q79[SQ01]`[i]) & !is.na(bdd_reference$`G06Q79[SQ02]`[i])) {
    
    # we obtain the mother's occupational group
    level_mere <-
      as.numeric(classe[which(classe[, 1] == bdd_reference$`G06Q79[SQ01]`[i]), 2])
    
    # we obtain the father's occupational group
    level_pere <-
      as.numeric(classe[which(classe[, 1] == bdd_reference$`G06Q79[SQ02]`[i]), 2])
    
    # keep the lowest level
    if (level_mere <= level_pere) {
      bdd_reference$CSP_highest[i] <- bdd_reference$`G06Q79[SQ01]`[i]
    } else {
      bdd_reference$CSP_highest[i] <- bdd_reference$`G06Q79[SQ02]`[i]
    }
    
    # we test whether the question on the mother's occupational group is not null and the father's is null
  } else if (!is.na(bdd_reference$`G06Q79[SQ01]`[i]) &
             is.na(bdd_reference$`G06Q79[SQ02]`[i])) {
    bdd_reference$CSP_highest[i] <- bdd_reference$`G06Q79[SQ01]`[i] # la CSP de la mère devient la CSP la plus haute
    
    # we test whether the question on the father's occupational group is not null and the mother's is null 
  } else if (is.na(bdd_reference$`G06Q79[SQ01]`[i]) &
             !is.na(bdd_reference$`G06Q79[SQ02]`[i])) {
    
    bdd_reference$CSP_highest[i] <- bdd_reference$`G06Q79[SQ02]`[i]# la CSP du père devient la CSP la plus haute
  }
  
}

#########################
### mother profession ###
#########################

bdd_reference$CSP_mother <- rep(NA, nrow(bdd_reference))

for (i in 1:nrow(bdd_reference)) {
  if (!is.na(bdd_reference$`G06Q79[SQ01]`[i])) {
    bdd_reference$CSP_mother[i] <- "1"
  }
  
} 

#########################
### father profession ###
#########################

bdd_reference$CSP_father <- rep(NA, nrow(bdd_reference))

for (i in 1:nrow(bdd_reference)) {
  if (!is.na(bdd_reference$`G06Q79[SQ02]`[i])) {
    bdd_reference$CSP_father[i] <- "1"
  }
  
}

####################
### profession 1 ###
####################

bdd_reference$CSP_1 <- rep(NA, nrow(bdd_reference))

for (i in 1:nrow(bdd_reference)) {
  if (!is.na(bdd_reference$CSP_highest[i])) {
    
    bdd_reference$CSP_1[i] <- bdd_reference$CSP_highest[i]
  } else {bdd_reference$CSP_1[i] <- bdd_reference$G06Q78[i]}
}

####################
### profession 2 ###
####################

bdd_reference$CSP_2 <- rep(NA, nrow(bdd_reference))

for (i in 1:nrow(bdd_reference)) {
  if (!is.na(bdd_reference$CSP_mother[i])) {
    
    bdd_reference$CSP_2[i] <- bdd_reference$`G06Q79[SQ01]`[i]
  } else {bdd_reference$CSP_2[i] <- bdd_reference$G06Q78[i]}
}

####################
### profession 3 ###
####################

bdd_reference$CSP_3 <- rep(NA, nrow(bdd_reference))

for (i in 1:nrow(bdd_reference)) {
  if (!is.na(bdd_reference$CSP_father[i])) {
    
    bdd_reference$CSP_3[i] <- bdd_reference$`G06Q79[SQ02]`[i]
  } else {bdd_reference$CSP_3[i] <- bdd_reference$G06Q78[i]}
}


# contributor -------------------------------------------------------------

bdd_reference$contrib <- rep(NA, nrow(bdd_reference))

bdd_reference[is.na(bdd_reference$`G04Q37[SQ01]`) & is.na(bdd_reference$`G04Q37[SQ02]`) & is.na(bdd_reference$`G04Q37[SQ03]`) & is.na(bdd_reference$`G04Q37[SQ04]`) & is.na(bdd_reference$`G04Q37[SQ05]`),]$contrib <- "AO01"

bdd_reference[is.na(bdd_reference$contrib),]$contrib <- "AO02"


# trust -------------------------------------------------------------------

# slow code depending on database size

#############
### trust ###
#############

bdd_reference$trust <- rep(NA, nrow(bdd_reference))

# trust ranking
classes <- c("AO01","AO02","AO03","AO04","AO05","AO06")
echelle <- 1:6
classe <- data.frame(classes,echelle)

# TODO : à accélérer
for (i in 1:nrow(bdd_reference)) {
  # we test whether the two confidence questions are non-null
  if (!is.na(bdd_reference$`G02Q27[SQ01]`[i]) & !is.na(bdd_reference$`G02Q27[SQ02]`[i])) {
    
    # we obtain the trust level for each question
    level_1 <-
      as.numeric(classe[which(classe[, 1] == bdd_reference$`G02Q27[SQ01]`[i]), 2])
    
    level_2 <-
      as.numeric(classe[which(classe[, 1] == bdd_reference$`G02Q27[SQ02]`[i]), 2])
    
    # we keep the average level
    if (level_1 != level_2) {
      
      moy <- round(mean(level_1,level_2),0)
      
      bdd_reference$trust[i] <- classe[moy, 1]
      
    } else {
      bdd_reference$trust[i] <- bdd_reference$`G02Q27[SQ02]`[i]
    }
    
    # we test whether the first trust question is non-null and the second null
  } else if (!is.na(bdd_reference$`G02Q27[SQ01]`[i]) &
             is.na(bdd_reference$`G02Q27[SQ02]`[i])) {
    bdd_reference$trust[i] <- bdd_reference$`G02Q27[SQ01]`[i] 
    
    # we test whether the second trust question is non-null and the first null
  } else if (is.na(bdd_reference$`G02Q27[SQ01]`[i]) &
             !is.na(bdd_reference$`G02Q27[SQ02]`[i])) {
    
    bdd_reference$trust[i] <- bdd_reference$`G02Q27[SQ02]`[i]
  }
  
}

bdd <- bdd_reference
